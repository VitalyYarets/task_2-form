(function () {
    let $render = document.querySelector('#render');

    function renderAllApp() {
        $render.innerHTML = ` <header class="header">
                                <button id="btn-login" class="button-login">login</button>
                              </header>
                              
                              <div class="allUsers" id="allUsers">          
                                   <ul class="users users__delete" id="users"></ul>
                              </div>`;
    }

    function renderAllAppStart() {
        renderAllApp();
        showAllUsers();
    }

    renderAllAppStart();
    events();

    const ROUTER = {
        registration: showRegistration,
        login: showLogin,
        main: renderAllAppStart,
        user: {
            show:  showUserEdit ,
            render: renderEditUser,
        },
    };

    console.log(ROUTER.user.show);
    console.log(ROUTER.user.render);

    let user = {};
    let $lastID = 0;
    let defaultPath = window.location.origin + window.location.pathname;

    history.replaceState({}, '', defaultPath + (window.location.hash ? window.location.hash : '#main'));
    window.location.hash = '#main'

    function getUser(id, withId = true) {
        let all_users = JSON.parse(localStorage.getItem("users"));
        if (all_users) {
            for (let user in all_users) {
                if (Object.keys(all_users[user])[0] == id) {
                    if (withId) {
                        return all_users[user];
                    }
                    return all_users[user][id];
                }
            }
        }
    }

    function saveUser(user, newUser = null, Id = null) {
        let oldUsers = JSON.parse(localStorage.getItem("users"));
        let result = [];
        if (oldUsers) {
            for (let users in oldUsers) {
                if (Object.keys(oldUsers[users])[0] == Id) {
                    oldUsers[users][Id] = user;
                }
                result.push(oldUsers[users]);
            }
        }
        if (newUser) {
            let curUser = {};
            getID();
            curUser[$lastID + 1] = Object.assign({}, user);
            result.push(curUser);
        }
        localStorage.setItem('users', JSON.stringify(result));
    }

    function getID() {
        let all_users = JSON.parse(localStorage.getItem("users"));
        let all_keys = [];
        if (all_users) {
            for (let user in all_users) {
                all_keys = all_keys.concat(Object.keys(all_users[user]));
            }
            $lastID = Math.max.apply(null, all_keys);
        }
    }

    function saveDateOnLocalStorageRegistration() {
        let $checkPasswords = document.querySelector('#checkpassword');
        let $email__registration = document.querySelector('#email__registration');
        let $password__registration = document.querySelector('#password');
        if ($email__registration.value && $password__registration.value !== '' && $password__registration.value === $checkPasswords.value) {
            user.name = $email__registration.value;
            user.password = $password__registration.value;
            saveUser(user, true);
            $email__registration.value = '';
            $password__registration.value = '';
            $checkPasswords.value = '';
            user = {};
        }
    }

    function showSpanUser(id) {
        let user = getUser(id, false);
        let a = 'User ' + id;
        a = `${user['name']}`;
        return `<span id="span" class="span delete show">${a}</span>`
    }

    function showAllUsers() {
        let $users = document.querySelector("#users");
        if (localStorage.getItem("users") !== null) {
            let storedNames = JSON.parse(localStorage.getItem('users'));

            for (let i = 0; i < storedNames.length; i++) {
                let li = document.createElement('li');
                li.classList.add('column');
                let curUserId = Object.keys(storedNames[i])[0];
                renderLiUsers(li, curUserId)
                $users.appendChild(li);
            }
        }
    }

    function renderLiUsers(list, id) {
        list.innerHTML = `<div class="container-btn">
                                ${showSpanUser(id)}
                                <button class="editBtn edit delete" id="${id}">edit</button>   
                                <button class="deleteBtn" id="${id}">delete</button>
                                </div>`
    }


    function checkPassword(e) {
        let password = document.querySelector('#password');
        let checkPasswords = document.querySelector('#checkpassword');
        let email__registration = document.querySelector('#email__registration');
        let password__registration = document.querySelector('#password');
        e.preventDefault();
        if (email__registration.value == '') {
            alert('Enter, please, mail')
        }
        if (password__registration.value == '') {
            alert('Enter, please, password')
        }

        if (password.value !== checkPasswords.value) {
            alert('passwords do not match');
        }
        saveDateOnLocalStorageRegistration();
    }

    function enterPage(e) {
        e.preventDefault();
        let $email__enter = document.querySelector('#email__enter');
        let $password__enter = document.querySelector('#password__enter')
        let all_users = JSON.parse(localStorage.getItem("users"));
        let keys = Object.keys(all_users);
        for (let i = 0; i < keys.length - 1; i++) {
            if ($email__enter.value == getUser(keys[i + 1], false).name && $password__enter.value == getUser(keys[i + 1], false).password) {
                location.href = 'https://rmsys.issoft.by/Home/Index';
            }
        }
    }

    function showLogin() {
        $render.innerHTML = ` <header class="header">
                                <button id="btn-login" class="button-login">login</button>
                                 </header>
                            <form id="form-enter" class="form-enter-flex" method="post">
            <div class="container">
             <div class="close" id="close"></div>
              <div class="arrow left" id="left"></div>
                <label>Email</label>
                <input id="email__enter" class="input__email" type="email" placeholder="enter email" name="email">
                <label>Password</label>
                <input id="password__enter" class='input__password' type="password" placeholder="Enter Password"
                       name="password">
               
                <button id="submit" class="submit register__button">Submit form</button>
            </div>
        </form>`
    }

    function showRegistration() {
        $render.innerHTML = `<header class="header">
                                 <button id="btn-login" class="button-login">login</button>
                             </header>
                             <form id="form-registration" class="form-registration" method="post">
                              
                                 <div class="container">
                                 <div class="close" id="close"></div>
                              <label>Email</label>
                <input id='email__registration' class='input__email' type="email" placeholder="you@example.com"
                       name="email">
                <label>Password</label>
                <input id="password" class='input__password' type="password" placeholder="Enter Password"
                       name="password">
                <label>Repeat password</label>
                <input id="checkpassword" class='input__repeat-password' type="password"
                       placeholder="Repeat password"
                       name="repeat password">
                <button id="btn" class="register__button">Enter</button>
                <button id="submit-registration" class="submit register__button">Submit registration</button>
            </div>
        </form>`
    }

    function closeModalWindow(e) {
        e.preventDefault();
        window.location.hash = '#main';
    }

    function addFormEnter(e) {
        e.preventDefault();
        window.location.hash = '#login';
    }

    function addFormModalWindow(e) {
        e.preventDefault();
        window.location.hash = '#registration';
    }

    function showUserEdit() {
        let target = event.target;
        let targetID = target.id;
        let hashID = `#user/${targetID}`;
        window.location.hash = hashID;
        renderEditUser(targetID);
        hashTarget(hashID, targetID);
    }

    function hashTarget(hash) {
        let k = hash.replace('#', '');
        ROUTER[k] = ROUTER[user];
        delete ROUTER.user;
        ROUTER[k] = renderEditUser;
    }

    function renderEditUser(id) {
        $render.innerHTML = `<div class = edit__render>
                           <input id="${id}" type="text" class="input__changeName" placeholder="please enter, name" value="${getUser(id, false)['name']}">
                           <button class="changeNameBtn" id="changeNameBtn">edit</button>
                           <button id="cancelBtn" class="edit delete cancelBtn">cancel</button> 
                           </div>`;
    }

    function editTargetUser(event) {
        let input = event.target.previousElementSibling;
        user.name = input.value;
        user.password = getUser(input.id, false)['password'];
        saveUser(user, null, input.id);
    }

    function deleteUserOnLocalStorage(event) {
        let target = event.target;
        let newArr = [];
        if (!target.classList.contains('delete')) {
            event.target.parentNode.hidden = !event.target.parentNode.hidden;
            let local = JSON.parse(localStorage.getItem("users"));
            for (let i = 0; i < local.length; i++) {
                if (Object.keys(local[i])[0] == target.id) {
                    continue;
                }
                newArr.push(local[i]);
            }
            localStorage.setItem('users', JSON.stringify(newArr))
        }
    }

    function hashChange() {
        let route = window.location.hash.replace('#', '');
        let param = '1';
        ROUTER[route](param);
    }

    function events() {
        $(document).on('click', '#submit-registration', checkPassword);
        $(document).on('click', '#btn', addFormEnter);
        $(document).on('click', '#btnEnter', addFormEnter);
        $(document).on('click', '#submit', enterPage);
        $(document).on('click', '#btn-login', addFormModalWindow);
        $(document).on('click', '.editBtn', showUserEdit);
        $(document).on('click', '.deleteBtn', deleteUserOnLocalStorage);
        $(document).on('click', '#close', closeModalWindow);
        $(document).on('click', '#left', addFormModalWindow);
        $(document).on('click', '#cancelBtn', closeModalWindow);
        $(document).on('click', '#changeNameBtn', editTargetUser);
        $(window).on('hashchange', hashChange);
    }
}());

